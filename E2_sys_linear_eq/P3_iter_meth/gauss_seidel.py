import numpy as np

def gauss_seidel(A, b, epsilon=1e-10):
    n = A.shape[0]
    x = np.zeros(n)
    iter = 0
    diff = float("Inf")
    while diff > epsilon:
        previous_x = x.copy()
        for i in range(0, n):
            x[i] = 1/A[i, i] * (b[i] - sum(A[i, 0:i] * x[0:i]) - sum(A[i, (i+1):n] * x[(i+1):n]))
        iter += 1
        diff = sum((x-previous_x)**2)
    return np.array([x])

if __name__ == "__main__":
    a = np.array(
        [[4, -1, 1],
         [-1, 4, -2],
         [1, -2, 4]], float
    )
    b = np.array([12, -1, 5], float)
    print(gauss_seidel(a, b))

