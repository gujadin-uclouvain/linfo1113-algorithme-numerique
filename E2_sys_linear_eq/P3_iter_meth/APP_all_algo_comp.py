import numpy as np
import time
from lu import lu_decomp, lu_solve
from lu_rp import lu_decomp_rp, lu_solve_rp
from gauss_seidel import gauss_seidel

"""
Question 1: Iterative methods:
--------------------------------------------------
[V] Gauss-Seidel requires less memory space than Jacobi.
[X] The Jacobi method (when it converges) is always faster than Gaussian elimination.
[X] Iterative methods always find the solution if one exists.
[V] Iterative methods are more robust regarding round-off errors than Gaussian elimination.
[V] Gaussian elimination always finds the solution if one exists.

**/--------------------------------------------------/**
**/--------------------------------------------------/**

Question 3: Algorithm comparison:
--------------------------------------------------
    Compute the inverse of the matrices defined in Question 8 of of the previous task
     (E2.2) using your Gauss-Seidel algorithm.
    
    Compare the error you get with Gauss-Seidel (ε=10−10 ) to those obtained using LU decomposition.
    
    Compare the CPU time difference to get the inverses using Gauss-Seidel and LU decomposition. Hint: To get visible 
     time difference repeat the inversion computation a large number of time (i.e. a thousand time).
--------------------------------------------------
[X] Gauss-Seidel is faster than LU decomposition for A1
[X] Gauss-Seidel is faster than LU decomposition for A2
[X] Gauss-Seidel is faster than LU decomposition for A3

[X] Gauss-Seidel has a lower error than LU decomposition for A1
[X] Gauss-Seidel has a lower error than LU decomposition for A2
[V] Gauss-Seidel has a lower error than LU decomposition for A3

[V] Gauss-Seidel doesn't converge for A1
[X] Gauss-Seidel doesn't converge for A2
[X] Gauss-Seidel doesn't converge for A3
"""

if __name__ == "__main__":
    a1 = np.array([
        [0.6, -0.4, 1],
        [-0.3, 0.2, 0.5],
        [0.6, -1, 0.5]], float)
    a2 = np.array([
        [1, 2, 4],
        [1, 3, 9],
        [1, 4, 16]], float)
    a3 = np.array([
        [4, -1, 0],
        [-1, 4, -1],
        [0, -1, 4]], float)
    b = np.array([1, 1, 1])

    def lu_test(a, b):
        start_time = time.time()
        for i in range(20000):
            lu_solve(lu_decomp(a.copy()), b.copy())
        print(" LU Test:          ", time.time() - start_time)
        print(lu_solve(lu_decomp(a.copy()), b.copy()))

    def lu_rp_test(a, b):
        start_time = time.time()
        for i in range(20000):
            lu, rp = lu_decomp_rp(a.copy())
            lu_solve_rp(lu, b.copy(), rp)
        print(" LU (Row Piv) Test:", time.time() - start_time)
        lu, rp = lu_decomp_rp(a.copy())
        print(lu_solve_rp(lu, b.copy(), rp))

    def seidel_test(a, b):
        start_time = time.time()
        for i in range(20000):
            gauss_seidel(a.copy(), b.copy())
        print(" Seidel Test:      ", time.time() - start_time)
        print(gauss_seidel(a.copy(), b.copy()))


    for a in [a2, a3]: # a1 doesn't work
        print("--- A ---\n", a)
        print()
        print("--- b ---\n", b)
        print("----------------------------------------------")
        lu_test(a, b)
        lu_rp_test(a, b)
        seidel_test(a, b)
        print()
        time.sleep(2)

