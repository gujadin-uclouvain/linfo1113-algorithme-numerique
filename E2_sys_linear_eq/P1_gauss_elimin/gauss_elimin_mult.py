import numpy as np
from gauss_elimin import gauss_elimin

# Question 4: Multiple vectors
def gauss_elimin_mult(a, b):
    return gauss_elimin(a, b)

if __name__ == "__main__":
    a = np.array(
        [[4, -2, 1],
         [-2, 4, -2],
         [1, -2, 4]], float
    )
    b = np.array(
        [[11, 11, 1],
         [-16, -20, 1],
         [17, 17, 1]], float
    )
    # print(gauss_elimin_mult(a, b))

    print("""
Question 5: Matrix inversion
---------------------------------------------------------
    Use this simultaneous solver (you can call the gauss_elimin_mult function you defined earlier)
    to compute the inverse of the matrix A.
    
    Verify that A^−1 * A = I
    
    Put the inverse of matrix A in variable 'a_inv' and the result of A^−1*A in variable 'a_ctrl'
---------------------------------------------------------
    """)
    print("--- A ---\n", a)
    print()
    a_inv = gauss_elimin_mult(a, np.identity(len(a)))
    a_ctrl = np.identity(len(a))  # A^-1 * A = I
    print("--- A^-1 ---\n", a_inv)
    print()
    print("--- A^-1 * A ---\n", a_ctrl)
