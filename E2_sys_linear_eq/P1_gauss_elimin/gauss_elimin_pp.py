import numpy as np
from gauss_elimin import gauss_elimin

# Question 6: Partial pivoting
def gauss_elimin_pp(a, b, should_pp=True):
    n = len(b)
    def swap_lines():
        max_index = line
        # Find maximum index
        for forward in range(line, n):
            if a[max_index][line] < a[forward][line]:
                max_index = forward
        # Swap lines 'l' and 'l+max_index' if !=
        if max_index != line:
            a[[line, line+max_index], :] = a[[line+max_index, line], :]

    # Forward substitution + Swap
    for line in range(n):
        if should_pp: swap_lines() # New
        pivot = a[line][line]
        a[line, :] /= pivot
        b[line] /= pivot
        for k in range(line + 1, n):
            pivot_down_to_zero = a[k][line]
            a[k, :] -= pivot_down_to_zero * a[line, :]
            b[k] -= pivot_down_to_zero * b[line]

    # Backwards substitution
    for line in range(n-2, -1, -1):
        for k in range(line, -1, -1):
            pivot_up_to_zero = a[k, line+1]
            a[k, :] -= pivot_up_to_zero * a[line+1, :]
            b[k] -= pivot_up_to_zero * b[line+1]

    return b

if __name__ == "__main__":
    a = np.array(
        [[0, -2, 2],
         [-2, 0, -20],
         [10, -2, 4]], float
    )
    b = np.array([1, 1, 1], float)

    print("--- A ---\n", a)
    print()
    print("--- b ---\n", b)
    print("----------------------------------------------")

    print("Partial Pivoting:\n", gauss_elimin_pp(a.copy(), b.copy()))
    print("----------------------------------------------")
    # wrong_algo = gauss_elimin(a, b)
    wrong_algo = "invalid value or divide by zero encountered"
    print("No Partial Pivoting:\n", wrong_algo)
