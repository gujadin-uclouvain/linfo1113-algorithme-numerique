import numpy as np

def gauss_elimin(a, b):
    n = len(b)

    # Forward substitution
    for line in range(n):
        pivot = a[line][line]
        a[line, :] /= pivot
        b[line] /= pivot
        for k in range(line+1, n):
            pivot_down_to_zero = a[k][line]
            a[k, :] -= pivot_down_to_zero * a[line, :]
            b[k] -= pivot_down_to_zero * b[line]

    # Backwards substitution
    for line in range(n-2, -1, -1):
        for k in range(line, -1, -1):
            pivot_up_to_zero = a[k, line+1]
            a[k, :] -= pivot_up_to_zero * a[line+1, :]
            b[k] -= pivot_up_to_zero * b[line+1]

    return b

if __name__ == "__main__":
    a = np.array(
        [[4, -2, 1],
         [-2, 4, -2],
         [1, -2, 4]], float
    )
    b = np.array([11, -16, 17], float)
    print(gauss_elimin(a, b))
