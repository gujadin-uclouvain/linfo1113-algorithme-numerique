import numpy as np

def lu_decomp_rp(a):
    """
    Improved LU decomposition with Doolittle's method included row pivoting
    :param a: a matrix
    :return: the [L/U] matrix of a
    :return: a reminder list which contains all the lines which was swapped
    """
    def swap_lines():
        pivot_index = line + list(np.abs(a[line:n, line])).index(max(np.abs(a[line:n, line])))
        a[line], a[pivot_index] = a[pivot_index], a[line].copy()
        rp_reminder.append(pivot_index)

    n = len(a)
    rp_reminder = [] # New
    for line in range(n-1): # n => n-1
        swap_lines() # New
        for k in range(line+1, n):
            if a[k, line] != 0.0:
                pivot_down_to_zero = a[k, line] / a[line, line] # a[line, line] => pivot
                a[k, line+1:] -= pivot_down_to_zero * a[line, line+1:]
                a[k, line] = pivot_down_to_zero
    return a, rp_reminder


def lu_solve_rp(lu, b, rp):
    def swap_blines_like_lu():
        for i in range(len(rp)): b[i], b[rp[i]] = b[rp[i]], b[i].copy()

    n = len(lu)
    swap_blines_like_lu() # New
    for line in range(1, n):
        b[line] -= np.dot(lu[line, 0:line], b[0:line])
    b[n-1] /= lu[n-1, n-1]
    for line in range(n-2, -1, -1):
        b[line] = (b[line] - np.dot(lu[line, line+1:], b[line+1:])) / lu[line, line]
    return b

if __name__ == '__main__':
    a = np.array(
        [[0.6, -0.4, 1],
         [-0.3, 0.2, 0.5],
         [0.6, -1, 0.5]], float
    )
    b = np.array([1, 1, 1], float)
    print("--- A ---\n", a)
    print()
    print("--- b ---\n", b)
    print("----------------------------------------------")
    lu, rp = lu_decomp_rp(a)
    print("LU Decomposition:\n", lu)
    print()
    print("Reminder Swap:\n", rp)
    print()
    print("LU Solve:\n", lu_solve_rp(lu, b, rp))
