import numpy as np
from lu import lu_decomp, lu_solve
from lu_rp import lu_decomp_rp, lu_solve_rp
from choleski import choleski_decomp, choleski_solve

def algo_comp(a, f=None):
    print()
    print("--- A ---\n", a)
    print("----------------------------------------------")
    if f == lu_decomp or f is None:
        lu = lu_decomp(a.copy())
        print("Doolitle Decomposition:\n", lu)
        print()
    if f == choleski_decomp or f is None:
        l = choleski_decomp(a.copy())
        print("Choleski Decomposition:\n", l)
    print("----------------------------------------------")
    print("----------------------------------------------")

def error_lu(a, b):
    print()
    print("--- A ---\n", a)
    print()
    print("--- b ---\n", b)
    print("----------------------------------------------")
    e_lu = np.max(a @ lu_solve(lu_decomp(a.copy()), b.copy()) - b)
    print("Error Without Row Pivoting:\n", e_lu)
    print()
    lu, rp = lu_decomp_rp(a.copy())
    a_inv = lu_solve_rp(lu, b.copy(), rp)
    e_lu_rp = np.max(a @ a_inv - b)
    print("Error With Row Pivoting:\n", e_lu_rp)
    print()
    print("----------------------------------------------")
    print("----------------------------------------------")

def APP_algo_comp():
    algo_comp(a1, lu_decomp)        # a1 is non-symmetric => Doolitle
    algo_comp(a2, lu_decomp)        # a2 is non-symmetric => Doolitle
    algo_comp(a3, choleski_decomp)  # a3 is     symmetric => Choleski

def APP_error_lu():
    error_lu(a1, b)
    error_lu(a2, b)
    error_lu(a3, b)

if __name__ == "__main__":
    a1 = np.array([[0.6, -0.4, 1], [-0.3, 0.2, 0.5], [0.6, -1, 0.5]], dtype=np.float32)
    a2 = np.array([[1, 2, 4], [1, 3, 9], [1, 4, 16]], dtype=np.float32)
    a3 = np.array([[4, -1, 0], [-1, 4, -1], [0, -1, 4]], dtype=np.float32)
    b = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]], dtype=np.float32)

    # APP_algo_comp()
    APP_error_lu()
