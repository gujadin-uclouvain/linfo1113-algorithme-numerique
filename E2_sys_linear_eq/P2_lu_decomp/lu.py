import numpy as np

def lu_decomp(a):
    """
    LU decomposition with Doolittle's method
    :param a: a matrix
    :return: the [L/U] matrix of a
    """
    n = len(a)
    for line in range(n):
        pivot = a[line, line]
        for k in range(line+1, n):
            if a[k, line] != 0.0:
                pivot_down_to_zero = a[k, line]/pivot
                a[k, line+1:] -= pivot_down_to_zero * a[line, line+1:]
                a[k, line] = pivot_down_to_zero # Store the multiplier in the lower triangular
    return a

def lu_solve(lu, b):
    n = len(lu)
    for line in range(1, n):
        b[line] -= np.dot(lu[line, 0:line], b[0:line])
    b[n-1] /= lu[n-1, n-1]
    for line in range(n-2, -1, -1):
        b[line] -= np.dot(lu[line, line+1:], b[line+1:])
        b[line] /= lu[line, line]

    return b


if __name__ == '__main__':
    a = np.array(
        [[3, -3, 3],
         [-3, 5, 1],
         [3, 1, 5]], float
    )
    b = np.array([9, -7, 12], float)
    print("--- A ---\n", a)
    print()
    print("--- b ---\n", b)
    print("----------------------------------------------")
    lu = lu_decomp(a)
    print("LU Decomposition:\n", lu)
    print()
    print("LU Solve:\n", lu_solve(lu, b))

