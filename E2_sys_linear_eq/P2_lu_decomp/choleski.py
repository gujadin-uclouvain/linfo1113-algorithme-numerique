import numpy as np

def choleski_decomp(a):
    """
    :param a: matrix a
    :return: a matrix with values in the lower triangle (named theoretically l)
    """
    n = len(a)
    for i in range(n):
        try:
            a[i, i] = np.sqrt(a[i, i] - np.sum(a[i, 0:i] * a[i, 0:i]))
        except ValueError:
            raise Exception('The matrix is not positive definite')

        for k in range(i+1, n):
            a[k, i] = (a[k, i] - np.dot(a[k, 0:i], a[i, 0:i])) / a[i, i]

    for i in range(1, n):  # Upper triangle to 0
        a[0:i, i] = 0.0

    return a

def choleski_solve(l, b):
    n = len(b)
    for i in range(n):  # Ly = b
        b[i] -= np.dot(l[i, 0:i], b[0:i])
        b[i] /= l[i, i]

    for i in range(n-1, -1, -1):  # L^t x = y
        b[i] -= np.dot(l[i+1:n, i], b[i+1:n])
        b[i] /= l[i, i]

    return b  # y

if __name__ == "__main__":
    a = np.array([[2, -1, 0], [-1, 2, -1], [0, -1, 2]], float)
    b = np.array([3, -1, 4], float)
    print("--- A ---\n", a)
    print()
    print("--- b ---\n", b)
    print("----------------------------------------------")
    l = choleski_decomp(a)
    print("Choleski Decomposition:\n", l)
    print()
    print("Choleski Solve:\n", choleski_solve(l, b))

