import numpy as np
from runge_kutta4 import RK4

def APP_ballistic():
    """
    Solve the ballistic problem using ODE.
    A plane with an horizontal velocity (along axis x) of x′=15m/s drops a bomb at an altitude of 2500m.
    The bomb is attracted by the ground according to Newton's gravity.
    It's acceleration along axis y is therefore y′′=−9.81.

    When and where will the bomb touch the ground ? (plane x position at dropping was 0)

    HINT: modify the RK4 algorithm to stop when y position is bellow 0
    """
    def should_stop(t, y): return y[1] < 0.0
    def F(t, xy): return np.array([15, xy[2], -9.81])

    t_start = 0.0
    xy_start = np.array([0.0, 2500.0, 0.0])
    t_stop = 25.0
    h = 0.005

    rk4_t, rk4_xy = RK4(F, t_start, xy_start, t_stop, h, should_stop=should_stop)
    rk4_x = rk4_xy[:, 0]
    rk4_y = rk4_xy[:, 1]

    return rk4_t, rk4_x, rk4_y

if __name__ == "__main__":
    rk4_t, rk4_x, rk4_y = APP_ballistic()
    print("""
Ballistic Problem with Runge-Kutta 4:
--------------------------------------------------------------------------------------------------------
    Solve the ballistic problem using ODE.
    A plane with an horizontal velocity (along axis x) of x′=15m/s drops a bomb at an altitude of 2500m.
    The bomb is attracted by the ground according to Newton's gravity.
    It's acceleration along axis y is therefore y′′=−9.81.

    When and where will the bomb touch the ground ? (plane x position at dropping was 0)
--------------------------------------------------------------------------------------------------------
    """)
    print("The plane will touch the ground after between", rk4_t[-2], "and", rk4_t[-1], "seconds")
    print("The impact position is", rk4_x[-2:].mean(), rk4_y[-2:].mean())

