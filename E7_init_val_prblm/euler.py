import numpy as np

def euler(F, x_start, y_start, x_stop, h):
    """
    Euler Formula: h*F(X[-1], Y[-1])
    'abs(xStop-x0) > 10**-12' is used instead of 'x < xStop' to avoid rounded value errors
    """
    def euler_formula(x, y): return h*F(x, y)
    X, Y = [x_start], [y_start]
    while abs(x_stop-X[-1]) > 10**-12:
        h = min(h, x_stop - X[-1])
        Y.append(Y[-1] + euler_formula(X[-1], Y[-1]))
        X.append(X[-1] + h)
    return np.array(X), np.array(Y)
