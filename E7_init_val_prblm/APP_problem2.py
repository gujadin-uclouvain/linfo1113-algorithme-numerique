import numpy as np
from euler import euler
from runge_kutta4 import RK4

def APP_problem2(algorithm):
    """
    Use your implementation to solve the following problem:
        y′′=-4×y with y(0)=1 and y′(0)=0 in the range[0, 5] in steps of 0.2.
    Store the x and y values into the vectors x and y, respectively.
    Compute the analytical solution and store the error in the vector e.
    :param algorithm: Euler (euler) or Runge-Kutta 4 (RK4)
    :return: x, y, e
    """

    def F(x, y):
        """
        :param x: a value x
        :param y: an array with [y(x), y′(x)]
        :return: an array with [y′(x), y′′(x)]
        """
        return np.array([y[1], -4*y[0]])

    x_start = 0.0
    y_start = np.array([1.0, 0.0])
    x_stop = 5.0
    h = 0.2

    x, y = algorithm(F, x_start, y_start, x_stop, h)
    y = y[:, 0]

    # ---------------------------------------------------------

    def analytical_f(x):
        """
        y′′ = y * cos(sqrt(4) * x)
        """
        return y_start[0] * np.cos(np.sqrt(4) * x)

    analytical_x = np.arange(x_start, x_stop + h, h)
    analytical_y = analytical_f(analytical_x)
    e = np.abs(analytical_y - y)  # error of the analytical solution

    return x, y, e

if __name__ == "__main__":
    euler_x, euler_y, euler_e = APP_problem2(euler)
    print("Problem n°2 with Euler: \ny′′ = -4×y \nwith y (0) = 1 \nand  y′(0) = 0 \nin the range[0, 5] \nin steps of "
          "0.2\n")
    print("x =", euler_x)
    print("y =", euler_y)
    print("e =", euler_e)
    print("------------------------------------")
    rk4_x, rk4_y, rk4_e = APP_problem2(RK4)
    print("Problem n°2 with Runge-Kutta 4: \ny′′ = -4×y \nwith y (0) = 1 \nand  y′(0) = 0 \nin the range[0, "
          "5] \nin steps of 0.2\n")
    print("x =", rk4_x)
    print("y =", rk4_y)
    print("e =", rk4_e)
