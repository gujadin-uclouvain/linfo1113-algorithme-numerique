import numpy as np
from euler import euler

def APP_problem1():
    """
    Use your implementation to solve the following problem:
        y′=0.5×y with y(0)=1 in the range[0, 5] in steps of 0.1.
    Store the x and y values into the vectors x and y, respectively.
    Compute the analytical solution and store the error in the vector e.
    :return x, y, e
    """

    def F(x, y): return 0.5 * y

    x_start = 0.0
    y_start = 1.0
    x_stop = 5.0
    h = 0.1

    x, y = euler(F, x_start, y_start, x_stop, h)

    # ---------------------------------------------------------

    def analytical_f(x):
        """
        y' = C*y will be always an exponential
        y' = C*y => y + e^{0.5*x}
        """
        return y_start + np.exp(0.5 * x)

    analytical_x = np.arange(x_start, x_stop + h, h)
    analytical_y = analytical_f(analytical_x)
    e = np.abs(analytical_y - y)  # error of the analytical solution

    return x, y, e

if __name__ == "__main__":
    x, y, e = APP_problem1()
    print("Problem n°1 with Euler: \ny′ = 0.5×y \nwith y (0) = 1 \nin the range[0, 5] \nin steps of 0.1\n")
    print("x =", x)
    print("y =", y)
    print("e =", e)
