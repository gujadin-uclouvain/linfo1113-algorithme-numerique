import numpy as np
from runge_kutta4 import RK4

def APP_spring_system():
    """
    The movement of a spring-mass system is defined by the following differential equation:
          mx′′(t) + cx′(t) + kx = 0
    Where:
        - x(t) is the body position at time t
        - t is the time variable
        - m is the mass of the body attached to the spring
        - c is the damping coefficient
        - k is the spring constant
    Can you solve the system (with your RK4 implementation) for
        - c = 0.95
        - k = 5
        - m = 1.5
        - x(0) = 10
        - x′(0) = 0
    for the timespan t∈[0,20] with time steps of 0.25.
    Store the t and x values into the vectors t and x, respectively.
    """
    m, c, k = 1.5, 0.95, 5.0
    def F(t, x):
        """
        :param t: a value t
        :param x: an array with [x(t), x′(t)]
        :return: an array with [x′(t), x′′(t)]
        """
        return np.array([x[1], -((c*x[1]) + k*x[0])/m])

    t_start = 0.0
    x_start = np.array([10.0, 0.0])
    t_stop = 20.0
    h = 0.25

    t, x = RK4(F, t_start, x_start, t_stop, h)
    x = x[:, 0]

    return t, x

if __name__ == "__main__":
    t, x = APP_spring_system()

    print("Spring System with Runge-Kutta 4: \nx′′ = -((c*x′) + k*x)/m \nwith x (0) = 10 \nand  x′(0) = 0 \nin the range[0, "
          "20] \nin steps of 0.25\n")
    print("t =", t)
    print("x =", x)
