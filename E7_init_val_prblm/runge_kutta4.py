import numpy as np

def RK4(F, x_start, y_start, x_stop, h, should_stop=None):
    """
    Better than Euler => Smaller errors variations
    Error order for each step is O(h^5) => accumulated error is O(h^4)
    """
    def stopper(x, y): return should_stop is not None and should_stop(x, y)
    def RK4_formula(x, y):
        K0 = h * F(x, y)
        K1 = h * F(x + h/2.0, y + K0/2.0)
        K2 = h * F(x + h/2.0, y + K1/2.0)
        K3 = h * F(x + h    , y + K2    )
        return (K0 + 2.0*K1 + 2.0*K2 + K3) / 6.0

    X, Y = [x_start], [y_start]
    while abs(x_stop-X[-1]) > 10**-12 and not stopper(X[-1], Y[-1]):
        h = min(h, x_stop - X[-1])
        Y.append(Y[-1] + RK4_formula(X[-1], Y[-1]))
        X.append(X[-1] + h)
    return np.array(X), np.array(Y)
