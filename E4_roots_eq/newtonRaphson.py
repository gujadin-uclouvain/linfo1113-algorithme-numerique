from rootsearch import rootsearch

def newtonRaphsonImproved(f, f_derivate, a, b, tol=1.0e-9):
    def newton_approx(x):
        try: return x - (f(x)/f_derivate(x))
        except ZeroDivisionError: return b - a
    def is_convergence(): return abs(x1-x0) < tol * max(abs(b), 1.0)
    def show_loops(): print(counter, "loops")
    if f(a) == 0.0: return a
    if f(b) == 0.0: return b
    x0 = 0.5 * (a+b)
    x1 = newton_approx(x0)
    counter = 0
    while not is_convergence():
        if f(x0) == 0.0: show_loops(); return x0, x1
        x0 = x1; x1 = newton_approx(x0)
        counter += 1
    show_loops(); return x0, x1

def newtonRaphson(f, f_derivate, a, b, tol=1.0e-9):
    def newton_approx(x, fx): return x - (fx/f_derivate(x))
    def is_convergence(): return abs(x1-x0) < tol
    def show_loops(): print(counter, "loops")
    x0 = 0.5 * (a+b); x1 = newton_approx(x0, f(x0))
    fx0 = f(x0)
    counter = 0
    while not is_convergence():
        x0 = x1; x1 = newton_approx(x0, fx0)
        counter += 1
    show_loops(); return x0, x1

def allnewtonRaphson(f, f_derivate, a, b, degree_of_df, tol=1.0e-9, newton_raphson_fun=newtonRaphson):
    roots = []
    counter = 0
    while (not len(roots) == degree_of_df) and (a < b):
        if counter > 200: raise TimeoutError
        root_val = round(newton_raphson_fun(f, f_derivate, a, b, tol)[1], 3)
        if len(roots) == 0: roots.append(root_val)
        else:
            if roots[-1] != root_val: roots.append(root_val)
        b -= 1; counter += 1
    roots.reverse()
    return roots

if __name__ == '__main__':
    def question_6():
        def fun(x): return x**4 + 2*x**3 - 7*x**2 + 3
        def dfun(x): return 4*x**3 + 6*x**2 - 14*x
        root = rootsearch(fun, -5, 5, 0.01)
        print(newtonRaphsonImproved(fun, dfun, root[0], root[1], 10e-8))

    def question_7():
        def fun(x): return x**3 - 1.2*x**2 - 8.19*x + 13.23
        def dfun(x): return 3*x**2 - 2.4*x - 8.19
        print(allnewtonRaphson(fun, dfun, -4, 5, 2, 10e-8, newtonRaphsonImproved))

    def question_9(): # Ne fonctionne pas -> Normal, objectif de l'exo
        def fun(x): return x**3 - 1.2*x**2 - 8*x + 15
        def dfun(x): return 3*x**2 - 2.4*x - 8
        print(allnewtonRaphson(fun, dfun, -4, 3, 2, 10e-8, newtonRaphson))

    def question_10():
        def fun(x): return x**3 - 1.2*x**2 - 8*x + 15
        def dfun(x): return 3*x**2 - 2.4*x - 8
        print(allnewtonRaphson(fun, dfun, -4, 3, 2, 10e-8, newtonRaphsonImproved))

    question_10()
