def rootsearch(f, a, b, dx):
    def is_opposite(fun1, fun2): return fun1*fun2 < 0
    x1, x2 = a, a + dx
    f1, f2 = f(x1), f(x2)
    counter = 0
    while not is_opposite(f1, f2):
        if x2 > b: return None, None
        x1, f1 = x2, f2
        x2 = x1 + dx; f2 = f(x2)
        counter += 1
    return x1, x2, counter

def allrootsearch(f, a, b, dx, degree, overflow=100):
    roots = []
    counter = 0
    while not len(roots) == degree:
        if counter > overflow: print("RootSearch did not find a root")
        root = rootsearch(f, a, b, dx)[1]
        if root is not None: root = round(root, 3)
        if len(roots) == 0: roots.append(root)
        else:
            if roots[-1] != root: roots.append(root)
        a += 1; counter += 1

    return roots

def f(x): return x ** 3 - 10 * (x ** 2) + 5
def g(x): return x ** 4 + 2 * (x ** 3) - 7 * (x ** 2) + 3

if __name__ == "__main__":
    # print(allrootsearch(g, -5, 4, 0.01, 4))
    r1 = rootsearch(f, 0, 1, 0.1)
    print(r1)
    r2 = rootsearch(f, r1[0], r1[1], 0.01)
    print(r2)
    r3 = rootsearch(f, r2[0], r2[1], 0.001)
    print(r3)
    r4 = rootsearch(f, 0, 1, 0.001)
    print(r4)
