from rootsearch import rootsearch, allrootsearch
import numpy as np
# Find approximately the position of a root

def bisection(f, x1, x2, tol=1.0e-9):
    def is_opposite(fun1, fun2): return fun1*fun2 < 0
    f1, f2 = f(x1), f(x2)
    while not abs(x2-x1) <= tol:
        x3 = 0.5 * (x1+x2)
        f3 = f(x3)
        if is_opposite(f1, f3): x2, f2 = x3, f3
        else: x1, f1 = x3, f3
    return 0.5 * (x1+x2)

def compute_roots(f, a, b, dx):
    stop = False
    print("The roots are:")
    if f(0) == 0: print(0.0)
    while not stop:
        x1, x2 = rootsearch(f, a, b, dx)
        if x1 is not None:
            a = x2
            root = bisection(f, x1, x2)
            if root is not None:
                print(root)
        else:
            print("\nDone")
            stop = True

def f(x): return x ** 3 - 10 * (x ** 2) + 5
def g(x): return x - np.tan(x)

if __name__ == "__main__":
    # root = rootsearch(f, 0.0, 1.0, 0.01)
    # print(bisection(f, root[0], root[1], 1e-8))

    # a, b, dx = (0., 1., 0.01)
    # compute_roots(f, a, b, dx)

    a, b, dx = (0.0, 20.0, 0.01)
    compute_roots(g, a, b, dx)
