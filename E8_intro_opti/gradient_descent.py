import numpy as np

def gradient_descent(eta, Fstar, gradF, xs=np.array((1, 1)), eta_fact=1, eps=10**-9):
    current_min = Fstar(xs[0], xs[1])
    next_min = 0
    while abs(current_min - next_min) > eps:
        current_min = next_min
        xs = xs - eta * gradF(xs[0], xs[1], eps)
        next_min = Fstar(xs[0], xs[1])
        eta = eta_fact * eta

    print("Next Minimum =", next_min)
    return xs[0], xs[1]


if __name__ == "__main__":
    def F(x, y): return 6*x**2 + y**3 + x*y
    def Fstar(x, y): return F(x, y) + Penalty(x, y)
    def dF_x(x, y, eps): return (Fstar(x + eps, y) - Fstar(x - eps, y)) / (2 * eps)
    def dF_y(x, y, eps): return (Fstar(x, y + eps) - Fstar(x, y - eps)) / (2 * eps)
    def gradF(x, y, eps): return np.array((dF_x(x, y, eps), dF_y(x, y, eps)))
    def Penalty(x, y):
        lam = 100.0
        c = np.minimum(0, y)  # Constraint function
        return lam * c ** 2

    print("Coordinates of the minimal point:\n", gradient_descent(0.1, Fstar, gradF))
    print("-----------------------------------------------")
    def Penalty(x, y):
        lam = 200
        c = np.minimum(0, y + 2)  # Constraint function
        return lam * c ** 2

    print("Coordinates of the minimal point:\n", gradient_descent(0.01, Fstar, gradF, np.array((0, -1)), eta_fact=0.99))
