import numpy as np
from bracket import bracket, visual_inspection, vectorize_f
import matplotlib.pyplot as plt
# Similar to the bisection algorithm
# Find approximately the position of a minimum

def golden(f, a, b, tol=1.0e-9):
    def new_x1(): return b-R*h # = R*a + C*b
    def new_x2(): return a+R*h # = C*a + R*b
    def new_h(): return b-a
    nbr_iter = int(np.ceil(-2.078087*np.log(tol/abs(b-a)))) # Compute the number of iteration golden needs
    R = 0.618033989
    C = 1.0 - R

    # First Telescoping
    h = new_h()
    x1, x2 = new_x1(), new_x2()
    fx1, fx2 = f(x1), f(x2)

    for _ in range(nbr_iter):
        if fx2 < fx1: # Reevaluate x2 => Go right
            a = x1
            h = new_h()
            x1, fx1 = x2, fx2
            x2 = new_x2(); fx2 = f(x2)
            if abs(x1-x2) < tol: return x2, fx2 # Replace nbr_iter computation
        else: # Reevaluate x1 => Go left
            b = x2
            h = new_h()
            x2, fx2 = x1, fx1
            x1 = new_x1(); fx1 = f(x1)
            if abs(x2-x1) < tol: return x1, fx1 # Replace nbr_iter computation

    if fx2 < fx1: return x2, fx2
    return x1, fx1

if __name__ == "__main__":
    f1, f2, f3, f4 = vectorize_f()

    for f in [f1, f2, f3, f4]:
        fig, axe = plt.subplots(figsize=(12, 8))
        visual_inspection(f, 0, 5, 0.1, color="blue", linestyle="-", label="f(x)")
        axe.legend()

        x_start = 2.5
        min_bracket = bracket(f, x_start, 0.01)

        if 0.0 <= min_bracket[0] and min_bracket[1] <= 5.0:
            axe.vlines(min_bracket, axe.get_ylim()[0], axe.get_ylim()[1], linewidth=3, color="black")
            print("Bracket Width =", np.abs(min_bracket[1] - min_bracket[0]))
            x_min, f_min = golden(f, min_bracket[0], min_bracket[1])
            axe.plot(x_min, f_min, "ok", markersize=5)
            print("     Minimum is", x_min)
            plt.show()
        else:
            print("No minimum find on the domain")
