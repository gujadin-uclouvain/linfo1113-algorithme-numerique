import numpy as np
import matplotlib.pyplot as plt
# Similar to the rootsearch algorithm but to find a minimum
# Always find a minimum because F(x).min == -F(x).max

def bracket(f, x1, h, c=1.618033989, overflow=100):
    def inc(x): temp = x+h; return temp, f(temp)
    fx1 = f(x1)
    x2, fx2 = inc(x1)

    # Determine downhill direction and change sign of 'h' if needed
    if fx1 < fx2: # if True: wrong way => go in the other direction
        h = -h
        x2, fx2 = inc(x1)
        if fx1 < fx2: return x2, x1-h # Already find a minimum

    # Searching Loop
    for _ in range(overflow):
        h *= c
        x3, fx3 = inc(x2)
        if fx2 < fx3: return x1, x3 # NOT x1, x2 !!!!
        x1, fx1, x2, fx2 = x2, fx2, x3, fx3
    print("Bracket did not find a minimum")

def visual_inspection(f, a, b, dx, **kwargs):
    x = np.arange(a, b + dx, dx)
    y = f(x)
    plt.plot(x, y, **kwargs)
    plt.xlabel('x')
    plt.ylabel('y')

def vectorize_f():
    def f1(x): return np.sin(3*x) + np.cos(x)
    def f2(x): return x**4 + (3-x)**3
    def f3(x): return x**3 - 5*(x-4)**3
    def f4(x): return 1 / (2 - np.cos(2*x) + 0.2*np.sin(2 + 8*x))
    return np.vectorize(f1), np.vectorize(f2), np.vectorize(f3), np.vectorize(f4)


if __name__ == "__main__":
    f1, f2, f3, f4 = vectorize_f()
    dom = (0.0, 5.0)

    for f in [f1, f2, f3, f4]:
        fig, axe = plt.subplots(figsize=(12, 8))
        visual_inspection(f, 0, 5, 0.1, color="blue", linestyle="-", label="f(x)")
        axe.legend()

        x_start = dom[1]/2
        min_bracket = bracket(f, x_start, 0.01)

        if dom[0] <= min_bracket[0] <= dom[1]:
            axe.vlines(min_bracket, axe.get_ylim()[0], axe.get_ylim()[1], linewidth=3, color="black")
            print("Bracket Width = ", np.abs(min_bracket[1] - min_bracket[0]))
            plt.show()
        else:
            print("No minimum find on the domain")
