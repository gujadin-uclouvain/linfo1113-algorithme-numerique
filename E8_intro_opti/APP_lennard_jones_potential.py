import numpy as np
from bracket import bracket
from golden_ratio import golden

def APP_lennard_jones_potential():
    """
    The Lennard-Jones potential between two molecules is
        V = 4ϵ[(σ/r)^12 − (σ/r)^6]
    where ϵ and σ are constants, and r is the distance between the molecules.
        1. Derivate the function to find its minimum analytically
        2. Use the goldSearch algorithm to find σ/r that minimizes the potential.

    Hint: you can use ϵ=1, as it won't change the minimum of the function, and you can visually look at the function
    in the domain [0, 1.5].
    """
    epsilon = 1.0
    def f(x): return 4*epsilon * (x**12 - x**6)

    x_start = 0.5
    min_bracket = bracket(f, x_start, 0.01)
    if 0.0 <= min_bracket[0] and min_bracket[1] <= 5.0:
        print("Bracket Width =", np.abs(min_bracket[1] - min_bracket[0]))
        x_min, f_min = golden(f, min_bracket[0], min_bracket[1])
        print("     Minimum x is", x_min)
        print("     Minimum f is", f_min)
    else:
        print("No minimum found on the domain")

if __name__ == "__main__":
    APP_lennard_jones_potential()
