import numpy as np
import matplotlib.pyplot as plt
from powell import powell
from downhill import downhill

def APP_problem(y_min_start=0):
    """
    Use the powell and downhill algorithm to solve the following problem:
        - Find the minimum of the function F(x,y) = 6x^2 + y^3 + xy subject to y ≥ y_min_start.
          Verify the result analytically.

    Hint: carefully chose the values of lambda (penalty scale factor) and use (0,0) as starting point.

    Report your answers following the format xmin, ymin.
    """

    def F(X):
        x, y = X[0], X[1]
        return 6*x**2 + y**3 + x*y

    def Penalty(X):
        x, y = X[0], X[1]
        # if lambda too lower when y_min_start < 0 => crash because Penalty too lower => no optimum
        lam = 1.0 if y_min_start >= 0.0 else 1000000.0
        c = np.minimum(0, y - y_min_start)  # Constraint function y
        return lam * c**2

    def Fstar(X): return F(X) + Penalty(X)

    fig = plt.figure(figsize=(16, 12))
    axe = fig.add_subplot(111)

    # show the surface
    X = np.arange(-5, 5, 0.1)
    Y = np.arange(-5, 5, 0.1)
    X, Y = np.meshgrid(X, Y)
    Z = Fstar(np.array([X, Y]))

    axe.cla()  # clear axes
    axe.set_xlabel('x')
    axe.set_ylabel('y')
    cs = axe.contourf(X, Y, Z, 50, cmap="rainbow_r")

    x_start = np.array([0, 0])

    print("RESULT WITH POWELL:")
    sequences = []
    XMin_PW = powell(Fstar, x_start, sequences=sequences)
    XMin_PW = XMin_PW[0]
    sequences = np.array(sequences)

    print("     x                =", XMin_PW)
    print("     F(x)             =", F(XMin_PW))
    print("     Number of cycles =", sequences.shape[0])
    print("     y                =", XMin_PW[0])
    print("     Penalty          =", Penalty(XMin_PW))
    axe.plot([XMin_PW], [F(XMin_PW)], "ob", markersize=2)
    plt.show()

    print("----------------------------------------------")
    print("RESULT WITH DOWNHILL SIMPLEX:")

    sequences = []
    XMin_DH = downhill(Fstar, x_start, sequences=sequences)
    sequences = np.array(sequences)

    print("     x                =", XMin_DH) # x_min, y_min
    print("     F(x)             =", F(XMin_DH))
    print("     Number of cycles =", sequences.shape[0])
    print("     y                =", XMin_DH[0])
    print("     Penalty          =", Penalty(XMin_DH))
    axe.plot([XMin_DH], [F(XMin_DH)], "ok", markersize=2)
    plt.show()
    print("----------------------------------------------")


if __name__ == "__main__":
    # g_i(x) = 0 => g_i(x) ** 2
    # g_i(x) = a => (g_i(x) - a) ** 2
    APP_problem(0)
    APP_problem(-2)
