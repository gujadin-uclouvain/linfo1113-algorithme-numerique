import numpy as np

h  = 0.01 # Difference between each x
xs = np.arange(2.36, 2.40+h, h)
ys = np.array(0.85866, 0.86289, 0.86710, 0.87129, 0.87680)

def first_center_p(xs, ys, x):
    index = xs.index(x)
    return (ys[index+1] - ys[index-1]) / (2*h)

def first_center_pp(xs, ys, x):
    index = xs.index(x)
    return (ys[index+1] + ys[index-1] - 2*ys[index]) / (h**2)

def first_fwd_p(xs, ys, x):
    index = xs.index(x)
    return (ys[index+1] - ys[index]) / h

def first_fwd_pp(xs, ys, x):
    index = xs.index(x)
    return (ys[index] - 2*ys[index+1] + ys[index+2]) / (h**2)

def second_fwd_p(xs, ys, x):
    index = xs.index(x)
    return (-3*ys[index] + 4*ys[index+1] - ys[index+2]) / (2*h)

def second_fwd_pp(xs, ys, x):
    index = xs.index(x)
    return (2*ys[index] - 5*ys[index+1] + 4*ys[index+2] - ys[index+3]) / (h**2)
