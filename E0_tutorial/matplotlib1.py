import numpy as np
from numpy import pi, cos, sin
import matplotlib.pyplot as plt

t = np.arange(-pi, pi, 0.01)
x = 16 * (sin(t)**3)
y = 13*cos(t) - 5*cos(2*t) - 2*cos(3*t) - cos(4*t)

plt.title("Love Function")
plt.plot(x, y, 'r-')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(('X&Y Function', ))
plt.grid(False)
plt.show()
