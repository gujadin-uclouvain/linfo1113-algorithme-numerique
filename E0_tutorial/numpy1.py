import numpy as np

#Exercise 1
a = np.arange(1, 5)
b = np.ones(4)

#Exercise 2
M = np.outer(a, b)
d = np.diag(M)
t = np.trace(M)

#Exercise 3
D = np.diagflat([1, 2, 3, 4])
N = np.subtract(M, D)
m_r = np.mean(N, 1)
m_c = np.mean(N, 0)
m_N = np.mean(m_r)
N_norm = N/np.linalg.norm(N)
