import numpy as np
import math

def serie_sin(x, n):
    if n <= 0:
        raise ValueError("n must be strictly bigger than 0")
    result = 0
    for i in range(0, n-1):
        result += ( ((-1)**i) / (math.factorial(2*i + 1)) ) * x**(2*i + 1)
    return result


def serie_exp(x, n):
    if n <= 0:
        raise ValueError("n must be strictly bigger than 0")
    result = 1
    for i in range(1, n):
        result += (x**i) / math.factorial(i)
    return result


def serie_ln(x, n):
    if n <= 0:
        raise ValueError("n must be strictly bigger than 0")
    result = x
    for i in range(1, n):
        result += ((-1)**i) * ((x**(i+1)) / (i+1))
    return result


if __name__ == "__main__":
    def choose_fun(f):
        """
        How many term would we need to sum in the following series
        to guarantee a truncation error bellow 10^−10:
        """
        if f == serie_sin:
            print("sin function:")
            print("     My Function:", f(1, 11))
            print("     Numpy:      ", np.exp(1))
        elif f == serie_exp:
            print("exp function:")
            print("     My Function:", f(np.pi, 14))
            print("     Numpy:      ", np.sin(np.pi))
        elif f == serie_ln:
            print("ln function:")
            print("     My Function:", f(0.99, 1495))  # pas de 1 car compris dans l'équation
            print("     Python:     ", math.log(1 + 0.99, math.e))
        print("----------------------------------------")

    choose_fun(serie_sin)
    choose_fun(serie_exp)
    choose_fun(serie_ln)
