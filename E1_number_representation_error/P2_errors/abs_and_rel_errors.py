import numpy as np
import matplotlib.pyplot as plt

x = np.arange(0.97, 1-(2*10**-5), 2*(10**-5))
f = np.array([
    np.sqrt(1-x[i]) for i in range(x.size)
])
e = np.array([
    ((1 / (2*f[i])) * 10**-3) for i in range(x.size) # f'(x) * 10^-3
])

plt.title("Absolute and relative errors (4/4)")
plt.xlabel("x")
plt.ylabel("f(x)")
plt.plot(x, f)
plt.plot(x, f+e)
plt.plot(x, f-e)
plt.legend(('f(x)', 'f(x) + |e_f(x)|', 'f(x)  -  |e_f(x)|'))
plt.show()
