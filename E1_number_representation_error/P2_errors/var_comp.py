import numpy as np


def var_comp(float_type):
    x = np.array([(10**6.1)+i for i in range(1, 8)], float_type)
    return (
        (1/7 * sum(x**2)) - (1/7 * sum(x))**2,
        (1/7) * sum((x - (1/7 * sum(x)))**2)
    )

if __name__ == "__main__":
    print("FLOAT32 :")
    r_32 = var_comp(np.float32)
    print("     ", r_32[0], "=> OVERFLOW")
    print("     ", r_32[1])
    print("---------------------------------------")
    print("FLOAT64 :")
    r_64 = var_comp(np.float64)
    print("     ", r_64[0])
    print("     ", r_64[1])
