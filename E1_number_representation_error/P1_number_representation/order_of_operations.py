import numpy as np

def naive_sum(array):
    return sum(array)

def smart_sum(array):
    array.sort()
    return naive_sum(array)

def exam_sum1(x, y, z):
    return (x+y+z)/3

def exam_sum2(x, y, z):
    return x/3 + y/3 + z/3

def exam_sum3(x, y, z):
    m = max(x, y)
    return m * (x/(3*m) + y/(3*m) + z/(3*m))


# a = np.array([1e16, 1, 1])
# print("Naive Sum:", naive_sum(a.copy()))
# print("Smart Sum:", smart_sum(a))

x, y, z = 1e100, 1e200, 1
print(exam_sum1(x, y, z))
print(exam_sum2(x, y, z))
print(exam_sum3(x, y, z))
