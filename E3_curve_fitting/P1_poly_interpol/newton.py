import numpy as np

def newton_coeffs(x_data, y_data):
    n = len(x_data)
    result_data = y_data.copy()
    for i in range(1, n):
        result_data[i:n] = (result_data[i:n] - result_data[i-1]) / (x_data[i:n] - x_data[i-1])
        # print(result_data[i:n]) # Show difference table
    return result_data

def newton_eval(a, x_data, x):
    d_poly = len(x_data) - 1  # -1 because we want the polynomial degree
    poly = a[d_poly]
    for i in range(1, d_poly+1):
        poly = a[d_poly-i] + (x - x_data[d_poly-i]) * poly
    return poly

if __name__ == '__main__':
    # x1 = -1.3
    # xdata = np.array([-1.2, 0.3, 1.1], float)
    # ydata = np.array([-5.76, -5.61, -3.69], float)
    # y_newton = newton_eval(newton_coeffs(xdata, ydata), xdata, x1)
    # print(y_newton)
    x1 = 9
    xdata = np.array([0, 5, 10], float)
    ydata = np.array([0, 3, 11], float)
    y_newton = newton_eval(newton_coeffs(xdata, ydata), xdata, x1)
    print(y_newton)
