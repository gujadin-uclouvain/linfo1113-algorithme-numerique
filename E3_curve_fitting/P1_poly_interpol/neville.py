import numpy as np

def neville(x_data, y_data, x):
    n = len(x_data)
    result_data = y_data.copy()

    for i in range(1, n):
        result_data[0:n-i] = ((x - x_data[i:n]) * result_data[0:n-i] + (x_data[0:n-i] - x) * result_data[1:n-i+1]) / (x_data[0:n-i] - x_data[i:n])

    return result_data[0]

if __name__ == '__main__':
    # x1 = -1.3
    # xdata = np.array([-1.2, 0.3, 1.1], float)
    # ydata = np.array([-5.76, -5.61, -3.69], float)
    # y_neuville = neville(xdata, ydata, x1)
    # print(y_neuville)

    x1 = 9
    xdata = np.array([0, 5, 10], float)
    ydata = np.array([0, 3, 11], float)
    y_neuville = neville(xdata, ydata, x1)
    print(y_neuville)
