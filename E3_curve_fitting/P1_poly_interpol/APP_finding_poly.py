import numpy as np
import matplotlib.pyplot as plt
from newton import newton_coeffs, newton_eval # src.E3.P1.

x_data = np.array([-2, 1, 4, -1, 3, -4])
y_data = np.array([-1, 2, 59, 4, 24, -53])
x = np.arange(np.min(x_data), np.max(x_data)+0.1, 0.1)

coeffs_newton = newton_coeffs(x_data, y_data)
y_newton = np.array([
    newton_eval(coeffs_newton, x_data, elem) for elem in x
])
print(coeffs_newton)

plt.title("Finding the polynomial (2/2)")
plt.xlabel("x")
plt.ylabel("f(x)")
plt.plot(x, y_newton, '-', label="Newton")
plt.plot(x_data, y_data, 'o', color="black")
plt.legend()
plt.show()
