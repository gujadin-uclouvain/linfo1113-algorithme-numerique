import numpy as np
import matplotlib.pyplot as plt
from newton import newton_coeffs, newton_eval # src.E3.P1.
from neville import neville # src.E3.P1.

x = np.arange(-1.3, 1.3, 0.1) # => [-1.3, 1.2]
xdata = np.array([-1.2, 0.3, 1.1], float)
ydata = np.array([-5.76, -5.61, -3.69], float)
y_newton = np.zeros(len(x))
y_neville = np.zeros(len(x))

for index_x, elem_x in enumerate(x):
    y_newton[index_x] = newton_eval(newton_coeffs(xdata, ydata), xdata, elem_x)
    y_neville[index_x] = neville(xdata, ydata, elem_x)

plt.title("Newton and Neville (5/5)")
plt.xlabel("x")
plt.ylabel("f(x)")
plt.plot(x, y_newton, '-')
plt.plot(x, y_neville, '--')
plt.plot(np.array([x[0], x[(len(x)-1)//2], x[len(x)-1]]), np.array([y_newton[0], y_newton[(len(y_newton)-1)//2], y_newton[len(y_newton)-1]]), 'o', color="black")
plt.legend(('f_newton(x)', 'f_neville(x)'))
plt.show()
