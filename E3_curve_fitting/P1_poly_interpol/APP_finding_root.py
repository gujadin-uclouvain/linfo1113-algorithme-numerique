import numpy as np
import matplotlib.pyplot as plt
from newton import newton_coeffs, newton_eval # src.E3.P1.

y_data = np.array([-1.5727, -0.4112, 0.8509, 1.9047, 2.4921], float)
x_data = np.array([3.0, 2.5, 2.0, 1.5, 1.0], float)
y = np.arange(np.min(y_data), np.max(y_data)+0.1, 0.1)

coeffs_newton = newton_coeffs(y_data, x_data)
x_newton = np.array([
    newton_eval(coeffs_newton, y_data, elem) for elem in y
])

plt.title("Finding the root (2/2)")
plt.xlabel("x")
plt.ylabel("f(x)")
plt.plot(y, x_newton, '-', label="Newton")
plt.plot(y_data, x_data, 'o', color="black")
plt.legend()
plt.show()
