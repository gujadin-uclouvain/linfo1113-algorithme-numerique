import numpy as np
from choleski import choleski_decomp, choleski_solve # src.E2.P2.

def poly_fit(x_data, y_data, degree):
    """
    Returns the coefficients of the polynomial of degree 'degree' which minimizes the least squared error on the
     training points represented by 'x_data' and 'y_data'.
    """
    n = degree+1
    a, b, arr_sum = np.zeros((n, n)), np.zeros(n), np.zeros(2*n)
    for i in range(len(x_data)):
        temp = y_data[i]
        for j in range(n):
            b[j] += temp
            temp *= x_data[i]
        temp = 1.0

        for j in range(2*n):
            arr_sum[j] += temp
            temp *= x_data[i]

    for i in range(n):
        for j in range(n):
            a[i, j] = arr_sum[i+j]

    return choleski_solve(choleski_decomp(a), b)

