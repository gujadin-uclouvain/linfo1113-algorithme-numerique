import numpy as np
import matplotlib.pyplot as plt
from poly_fit import poly_fit # src.E3.P3.

x_data = np.array([1.0, 2.5, 3.5, 4.0, 1.1, 1.8, 2.2, 3.7])
y_data = np.array([6.008, 15.722, 27.130, 33.772, 5.257, 9.549, 11.098, 28.828])
x = np.arange(1, 4.1, 0.1)

poly_lin = poly_fit(x_data, y_data, 1)
poly_quadr = poly_fit(x_data, y_data, 2)
poly_7 = poly_fit(x_data, y_data, 7)

y_linear = poly_lin[1]*x + poly_lin[0]
y_quadratic = poly_quadr[2]*(x**2) + poly_quadr[1]*x + poly_quadr[0]
y_7 = poly_7[7]*(x**7) + poly_7[6]*(x**6) + poly_7[5]*(x**5) + poly_7[4]*(x**4) + poly_7[3]*(x**3) + poly_7[2]*(x**2) + poly_7[1]*x + poly_7[0]

plt.title("Polynomial Regression (2/2)")
plt.xlabel("x")
plt.ylabel("y")
plt.plot(x_data, y_data, 'o', color="black")
plt.plot(x, y_linear, '-', color="blue", label="Linear")
plt.plot(x, y_quadratic, '--', color="orange", label="Quadratic")
plt.plot(x, y_7, '-.', color="green", label="Degree 7")
plt.legend()
plt.show()

