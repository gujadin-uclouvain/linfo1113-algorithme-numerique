import numpy as np

def lu3_decomp(c, d, e):
    n = len(d)
    for i in range(1, n):
        lam = c[i - 1] / d[i - 1]
        d[i] -= lam * e[i - 1]
        c[i - 1] = lam
    return c, d, e

def lu3_solve(c, d, e, b):
    n = len(d)
    for i in range(1, n):
        b[i] -= c[i - 1] * b[i - 1]
    b[n - 1] /= d[n - 1]
    for i in range(n - 2, -1, -1):
        b[i] = (b[i] - e[i] * b[i + 1]) / d[i]
    return b

