import numpy as np
import matplotlib.pyplot as plt
from gauss_elimin import gauss_elimin
from cubicspline import cubicspline_curvatures, cubicspline_eval

x_data = np.array([0, 1, 2], float)
y_data = np.array([0, 2, 1], float)
x = np.arange(0, 2.01, 0.01)

# Cubic spline (k0, kn == 0)
curv_spline = cubicspline_curvatures(x_data, y_data)
y_spline = np.array([
    cubicspline_eval(x_data, y_data, curv_spline, elem) for elem in x
])

# Gauss elimin result matrix for cubic spline with border conditions (k0, kn != 0)
curv_spline_border = gauss_elimin(
    np.array([[-2, -1, 0], [1, 4, 1], [0, 1, 2]], float),
    np.array([-12, -18, 6], float)
)

y_spline_border = np.array([
    cubicspline_eval(x_data, y_data, curv_spline_border, elem) for elem in x
])

plt.title("Cubic spline with border conditions (2/2)")
plt.xlabel("x")
plt.ylabel("f")
plt.plot(x, y_spline, '-')
plt.plot(x, y_spline_border, '--')
plt.plot(x_data, y_data, 'o', color="black")
plt.legend(['f_spline(x)', "f_spline(x) [f'(x_0)=f(x_2)=0]"])
plt.show()
