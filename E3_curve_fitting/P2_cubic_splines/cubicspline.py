import numpy as np
import matplotlib.pyplot as plt
from newton import newton_coeffs, newton_eval
from lu3 import lu3_decomp, lu3_solve

def cubicspline_curvatures(x_data, y_data):
    n = len(x_data) - 1
    c, d, e, b = np.zeros(n), np.ones(n+1), np.zeros(n), np.zeros(n+1)
    c[:n-1] = x_data[:n-1] - x_data[1:n]
    d[1:n] = 2.0 * (x_data[:n-1] - x_data[2:n+1])
    e[1:n] = x_data[1:n] - x_data[2:n+1]
    b[1:n] = 6.0 * (y_data[:n-1] - y_data[1:n]) / (x_data[:n-1] - x_data[1:n]) \
           - 6.0 * (y_data[1:n] - y_data[2:n+1]) / (x_data[1:n] - x_data[2:n+1])
    lu3_decomp(c, d, e)
    lu3_solve(c, d, e, b)
    return b


def cubicspline_eval(xData, yData, k, x):
    left, right, found, i = 0, len(xData)-1, False, None
    while not found: # Find Segment
        if right-left <= 1:
            found = True
            i = left
        else:
            center = int((left+right)/2)
            if x < xData[center]: right = center
            else: left = center

    h = xData[i] - xData[i+1]
    eq1 = x - xData[i]
    eq2 = x - xData[i+1]
    return ( eq2**3/h     - eq2*h)     * k[i]   / 6.0 \
         - ( eq1**3/h     - eq1*h)     * k[i+1] / 6.0 \
         + ( yData[i]*eq2 - yData[i+1] * eq1 )  / h


if __name__ == "__main__":
    x_data = np.array([2, 3, 4], float)
    y_data = np.array([4, 5, 4], float)
    x = np.arange(np.min(x_data), np.max(x_data)+0.1, 0.1)

    curv_spline = cubicspline_curvatures(x_data, y_data)
    y_spline = np.array([
        cubicspline_eval(x_data, y_data, curv_spline, elem) for elem in x
    ])
    #
    # n_coeffs = newton_coeffs(x_data, y_data)
    # y_newton = np.array([
    #     newton_eval(n_coeffs, x_data, elem) for elem in x
    # ])

    plt.title("Cubic spline implementation (4/4)")
    plt.xlabel("x")
    plt.ylabel("f")
    # plt.plot(x, y_newton, '-')
    plt.plot(x, y_spline, '--')
    plt.plot(x_data, y_data, 'o', color="black")
    plt.legend("f_spline")
    # plt.legend(['f_newton(x)', "f_spline"])
    plt.show()

