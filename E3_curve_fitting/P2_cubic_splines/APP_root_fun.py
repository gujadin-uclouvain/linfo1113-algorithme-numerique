import numpy as np
import matplotlib.pyplot as plt
from cubicspline import cubicspline_curvatures, cubicspline_eval

x_data = np.array([0.2, 0.4, 0.6, 0.8, 1])
y_data = np.array([1.15, 0.855, 0.377, -0.266, -1.049])
x = np.arange(1, 0, -0.2)

curv_spline = cubicspline_curvatures(x_data, y_data)
y_spline = np.array([
    cubicspline_eval(x_data, y_data, curv_spline, elem) for elem in x
])
print(y_spline)
