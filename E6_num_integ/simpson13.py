import numpy as np

def simpson(f, a, b, n, counter=False):
    def counter_is_on(): return counter != False or type(counter) == int
    if counter_is_on(): counter = 1
    integral = 0
    h = (b-a)/n
    x = a
    for _ in range(0, n, 2):
        integral += f(x) + 4*f(x+h) + f(x + 2*h)
        x += 2*h
        if counter_is_on(): counter += 1

    return ((h/3) * integral, counter) if counter_is_on() else (h/3) * integral


if __name__ == "__main__":
    def f(x):
        return np.cos(2/(np.cos(x)))

    print("2 Panels     =>", simpson(f, -1, 1, 2))
    print("4 Panels     =>", simpson(f, -1, 1, 4))
    print("6 Panels     =>", simpson(f, -1, 1, 6))
    print("8 Panels     =>", simpson(f, -1, 1, 8))
    print("10 Panels    =>", simpson(f, -1, 1, 10))
    print("12 Panels    =>", simpson(f, -1, 1, 12))
    print("14 Panels    =>", simpson(f, -1, 1, 14))
    print("16 Panels    =>", simpson(f, -1, 1, 16))
    print("50 Panels    =>", simpson(f, -1, 1, 50))
    print("100 Panels   =>", simpson(f, -1, 1, 100))
    print("1000 Panels  =>", simpson(f, -1, 1, 1000))
