import numpy as np
from romberg import romberg

x = np.array([0, np.pi/4, np.pi/2, 3*np.pi/4, np.pi])
y = np.array([1, 0.3431, 0.25, 0.3431, 1])

def f(x):
    if x == 0.0: return 1
    elif x == np.pi/4: return 0.3431
    elif x == np.pi/2: return 0.25
    elif x == 3*np.pi/4: return 0.3431
    elif x == np.pi: return 1
    raise ValueError(x, "is not an acceptable value")

f = np.vectorize(f)
R = romberg(f, 0, np.pi, 3, matrix=True)
print("Diagonal =>", np.diag(R))
print("R_{3, 3} =>", R[2, 2])
