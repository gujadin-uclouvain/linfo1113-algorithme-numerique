import numpy as np

def trapezoid_comp(f, a, b, n, counter=False):
    def counter_is_on(): return counter != False or type(counter) == int
    if counter_is_on(): counter = 1
    integral = 0
    h = (b-a)/n
    x, fx = a, f(a)
    for _ in range(n):
        fxh = f(x + h)
        integral += fx + fxh
        x, fx = x+h, fxh
        if counter_is_on(): counter += 1

    return ((h / 2) * integral, counter) if counter_is_on() else (h / 2) * integral

def trapezoid_rec(f, a, b, k):
    """
    No additional cost BUT can monitor convergence by comparing I_k and I_k-1 (stop earlier)
    The number of Panels n = 2^{k-1}
    Example: if n == 16 do k = 5
    """
    if k == 1: return (f(a) + f(b)) * ((b-a)/2)
    n = 2**(k-2)        # k=2 => n=1            --> k=3 => n=2
    h = (b-a)/(2*n)     # k=2 => (b-a)/2        --> k=3 => (b-a)/4
    x = a + h           # a + (b-a)/2 = (a+b)/2 --> a + (b-a)/4
    sfx = 0
    for _ in range(n):
        sfx += f(x)     # evaluate the function at the new point
        x += 2*h        # go to the new next point
    return trapezoid_rec(f, a, b, k-1)/2 + h*sfx

if __name__ == "__main__":
    def f(x):
        return np.log(1 + np.tan(x))

    composite_approx, composite_count = trapezoid_comp(f, 0, np.pi/4, 16, counter=True)
    recursive_approx, recursive_count = trapezoid_rec(f, 0, np.pi/4, 5), composite_count

    print(composite_approx)
    print(composite_count)
    print(recursive_approx)
    print(recursive_count) # Same as composite_count
