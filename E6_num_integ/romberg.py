import numpy as np
from simpson13 import simpson
from trapezoid import trapezoid_comp

def romberg(f, a, b, n, counter=False, matrix=False):
    """
    Better than Simpson 1/3 => Reduce error
    :return (matrix == False) R_{n,n}
    :return (matrix == True)  R
    """
    def counter_is_on(): return counter != False or type(counter) == int
    if counter_is_on(): counter = 2
    h = b-a
    R = np.zeros(shape=(n, n))
    R[0, 0] = (f(a) + f(b)) / (h/2)
    for i in range(2, n+1):
        line_i = np.arange(1, 2**(i-2)+1)
        points = a + (2 * line_i - 1) * (h/(2**(i-1)))
        s = h/(2**(i-1)) * np.sum(f(points))
        R[i-1, 0] = R[i-2, 0]/2 + s

        for k in range(2, i+1):
            # Richardson extrapolation formula
            R[i-1, k-1] = (4**(k-1) * R[i-1, k-2] - R[i-2, k-2]) / (4**(k-1)-1)
        if counter_is_on(): counter += 1

    return ((R, counter) if counter_is_on() else R) if matrix else ((np.diag(R)[n-1], counter) if counter_is_on() else np.diag(R)[n-1])


if __name__ == "__main__":
    def f(x):
        return np.cos(2/(np.cos(x)))

    print("Romberg     (BETTER)     =>", romberg(f, -1, 1, 16))
    print("Simpson 1/3              =>", simpson(f, -1, 1, 5000))
    print("Composite Trapezoidal    =>", trapezoid_comp(f, -1, 1, 1000000))
