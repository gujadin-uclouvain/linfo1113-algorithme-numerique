import numpy as np
from romberg import romberg

x = np.array([3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7], float)
y = np.array([1.2, 2.3, 3.5, 4.7, 5.9, 6.7, 7, 7.2, 7.4], float)

def f(x1):
    for i in range(0, len(x)):
        if x[i] == x1: return y[i]
    raise ValueError(x, "is not an acceptable value")

f = np.vectorize(f)
R = romberg(f, 3, 7, 4, matrix=True)
print("Diagonal =>", np.diag(R))
print("R_{3, 3} =>", R)
